import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from DetImages import DetDataset, DetConfig, PredictionConfig, DetImages
from mrcnn.model import MaskRCNN
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from mrcnn.utils import compute_ap
import numpy as np


# create config
cfg = PredictionConfig()

# define the model
model = MaskRCNN(mode='inference', model_dir='./', config=cfg)

# load model weights
weights_path = 'D:\\deep_learning_vending_machines\\vending_machines\\vending_cfg20201030T1703\\mask_rcnn_vending_cfg_0005.h5'
model.load_weights(weights_path, by_name=True)

images_dir = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\JPEGImages'
annotations_dir ='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\Annotations'

# load the train dataset
train_set = DetDataset()
train_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\train.txt'
train_set.load_dataset(train_set_file, images_dir, annotations_dir)
train_set.prepare()

# load the test dataset
test_set = DetDataset()
test_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\test.txt'
test_set.load_dataset(test_set_file, images_dir, annotations_dir)
test_set.prepare()

# load image, bounding boxes and masks for the image id
#image_id = 200
#image, image_meta, gt_class_id, gt_bbox, gt_mask = load_image_gt(test_set, cfg, image_id, use_mini_mask=False)

# convert pixel values (e.g. center)
#scaled_image = mold_image(image, cfg)

#sample = np.expand_dims(scaled_image, 0)
# make prediction
#yhat = model.detect(sample, verbose=0)
# extract results for first sample
#r = yhat[0]

# calculate statistics, including AP
#AP, _, _, _ = compute_ap(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["scores"], r['masks'])

det = DetImages()

# evaluate model on training dataset
train_mAP = det.evaluate_model(train_set, model, cfg, 5)
print("Train mAP: %.3f" % train_mAP)
# evaluate model on test dataset
test_mAP = det.evaluate_model(test_set, model, cfg, 5)
print("Test mAP: %.3f" % test_mAP)

