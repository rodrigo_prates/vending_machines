# example of extracting bounding boxes from an annotation file
from xml.etree import ElementTree
import os
from mrcnn.utils import Dataset
import numpy as np
from mrcnn.config import Config
#from mrcnn.model import MaskRCNN
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from mrcnn.utils import compute_ap
from matplotlib import pyplot
from matplotlib.patches import Rectangle


class DetDataset(Dataset):

    #def __init__(self):
    #    pass

    # function to extract bounding boxes from an annotation file
    def extract_boxes(self, filename):
	    # load and parse the file
        tree = ElementTree.parse(filename)
        # get the root of the document
        root = tree.getroot()
        # extract each bounding box
        boxes = list()
        classnames = list()
        for obj in root.iter('object'):
            classnames.append(obj.find('name').text)
            xmlbox = obj.find('bndbox')
            xmin = int(xmlbox.find('xmin').text)
            ymin = int(xmlbox.find('ymin').text)
            xmax = int(xmlbox.find('xmax').text)
            ymax = int(xmlbox.find('ymax').text)
            coors = [xmin, ymin, xmax, ymax]
            boxes.append(coors)
        # extract image dimensions
        width = int(root.find('.//size/width').text)
        height = int(root.find('.//size/height').text)
        return boxes, classnames, width, height

    # load the dataset definitions
    def load_dataset(self, set_file, images_dir, annotations_dir):
        # define classes
        self.add_class("dataset", 1, "fenda")
        self.add_class("dataset", 2, "yingyangkuaixian")
        self.add_class("dataset", 3, "jiaduobao")
        self.add_class("dataset", 4, "maidong")
        self.add_class("dataset", 5, "TYCL")
        self.add_class("dataset", 6, "BSS")
        self.add_class("dataset", 7, "TYYC")
        self.add_class("dataset", 8, "LLDS")
        self.add_class("dataset", 9, "KSFH")
        self.add_class("dataset", 10, "MZY")

        with open(set_file) as f:
            filenames = f.readlines()
        filenames = [x.strip() for x in filenames] 

        # find all images
        for filename in filenames:
            # extract image id
            image_id = filename
            
            img_path = images_dir + '\\' + filename + '.jpg'
            ann_path = annotations_dir + '\\' + image_id + '.xml'
            # add to dataset
            self.add_image('dataset', image_id=image_id, path=img_path, annotation=ann_path)

    # load the masks for an image
    def load_mask(self, image_id):
        # get details of image
        info = self.image_info[image_id]
        # define box file location
        path = info['annotation']
        # load XML
        boxes, classnames, w, h = self.extract_boxes(path)
        # create one array for all masks, each on a different channel
        masks = np.zeros([h, w, len(boxes)], dtype='uint8')
        # create masks
        class_ids = list()
        for i in range(len(boxes)):
            box = boxes[i]
            row_s, row_e = box[1], box[3]
            col_s, col_e = box[0], box[2]
            masks[row_s:row_e, col_s:col_e, i] = 1
            class_ids.append(self.class_names.index(classnames[i]))
        return masks, np.asarray(class_ids, dtype='int32')

    # load an image reference
    def image_reference(self, image_id):
        info = self.image_info[image_id]
        return info['path']


# define a configuration for the model
class DetConfig(Config):

    # Give the configuration a recognizable name
    NAME = "vending_cfg"
    # Number of classes (background + products)
    NUM_CLASSES = 10 + 1
    # Number of training steps per epoch
    #BATCH_SIZE = 8
    STEPS_PER_EPOCH = 300
    IMAGES_PER_GPU = 1


# define the prediction configuration
class PredictionConfig(Config):

    # define the name of the configuration
    NAME = "vending_cfg"
    # number of classes (background + kangaroo)
    NUM_CLASSES = 10 + 1
    # simplify GPU config
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


class DetImages:

    # calculate the mAP for a model on a given dataset
    def evaluate_model(self, dataset, model, cfg, limit=5):
        APs = list()
        count=0
        for image_id in dataset.image_ids:
            # load image, bounding boxes and masks for the image id
            image, image_meta, gt_class_id, gt_bbox, gt_mask = load_image_gt(dataset, cfg, image_id, use_mini_mask=False)
            # convert pixel values (e.g. center)
            scaled_image = mold_image(image, cfg)
            # convert image into one sample
            sample = np.expand_dims(scaled_image, 0)
            # make prediction
            yhat = model.detect(sample, verbose=0)
            # extract results for first sample
            r = yhat[0]
            # calculate statistics, including AP
            AP, _, _, _ = compute_ap(gt_bbox, gt_class_id, gt_mask, r["rois"], r["class_ids"], r["scores"], r['masks'])
            # store
            APs.append(AP)

            count += 1
            if count > limit:
                break
        # calculate the mean AP across all images
        mAP = np.mean(APs)
        return mAP

    # plot a number of photos with ground truth and predictions
    def plot_actual_vs_predicted(self, dataset, model, cfg, n_images=5):
        # load image and mask
        for i in range(n_images):
            # load the image and mask
            image = dataset.load_image(i)
            mask, _ = dataset.load_mask(i)
            # convert pixel values (e.g. center)
            scaled_image = mold_image(image, cfg)
            # convert image into one sample
            sample = np.expand_dims(scaled_image, 0)
            # make prediction
            yhat = model.detect(sample, verbose=0)[0]
            # define subplot
            pyplot.subplot(n_images, 2, i*2+1)
            # plot raw pixel data
            pyplot.imshow(image)
            pyplot.title('Actual')
            # plot masks
            for j in range(mask.shape[2]):
                pyplot.imshow(mask[:, :, j], cmap='gray', alpha=0.3)
            # get the context for drawing boxes
            pyplot.subplot(n_images, 2, i*2+2)
            # plot raw pixel data
            pyplot.imshow(image)
            pyplot.title('Predicted')
            ax = pyplot.gca()
            # plot each box
            for box in yhat['rois']:
                # get coordinates
                y1, x1, y2, x2 = box
                # calculate width and height of the box
                width, height = x2 - x1, y2 - y1
                # create the shape
                rect = Rectangle((x1, y1), width, height, fill=False, color='red')
                # draw the box
                ax.add_patch(rect)
        # show the figure
        pyplot.show()