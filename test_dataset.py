from DetImages import DetDataset
from matplotlib import pyplot
from mrcnn.visualize import display_instances
from mrcnn.utils import extract_bboxes


images_dir = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\JPEGImages'
annotations_dir ='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\Annotations'

det = DetDataset()

# train set
set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\train.txt'
det.load_dataset(set_file, images_dir, annotations_dir)
det.prepare()
print('Train: %d' % len(det.image_ids))

# load an image from traindata
image_id = 0
image = det.load_image(image_id)
print(image.shape)
# load image mask
mask, class_ids = det.load_mask(image_id)
print(mask.shape)
# plot image
pyplot.imshow(image)
# plot mask
pyplot.imshow(mask[:, :, 0], cmap='gray', alpha=0.5)
pyplot.show()

# extract bounding boxes from the masks
bbox = extract_bboxes(mask)
# display image with masks and bounding boxes
display_instances(image, bbox, mask, class_ids, det.class_names)

# plot first few images
for i in range(9):
	# define subplot
	pyplot.subplot(330 + 1 + i)
	# plot raw pixel data
	image = det.load_image(i)
	pyplot.imshow(image)
	# plot all masks
	mask, _ = det.load_mask(i)
	for j in range(mask.shape[2]):
		pyplot.imshow(mask[:, :, j], cmap='gray', alpha=0.3)
# show the figure
pyplot.show()

# enumerate all images in the dataset
#for image_id in det.image_ids:
	# load image info
#	info = det.image_info[image_id]
	# display on the console
#	print(info)
 
det = DetDataset()

# test set
set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\test.txt'
det.load_dataset(set_file, images_dir, annotations_dir)
det.prepare()
print('Test: %d' % len(det.image_ids))