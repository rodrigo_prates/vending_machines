import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

from DetImages import DetDataset, DetConfig, PredictionConfig, DetImages
from mrcnn.model import MaskRCNN
from mrcnn.model import load_image_gt
from mrcnn.model import mold_image
from mrcnn.utils import compute_ap
import numpy as np


cfg = PredictionConfig()
# define the model
model = MaskRCNN(mode='inference', model_dir='./', config=cfg)
# load model weights
weights_path = 'D:\\deep_learning_vending_machines\\vending_machines\\vending_cfg20201101T1211\\mask_rcnn_vending_cfg_0008.h5'
model.load_weights(weights_path, by_name=True)

det = DetImages()

images_dir = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\JPEGImages'
annotations_dir ='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\Annotations'

# load the train dataset
train_set = DetDataset()
train_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\train.txt'
train_set.load_dataset(train_set_file, images_dir, annotations_dir)
train_set.prepare()

# load the test dataset
test_set = DetDataset()
test_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\test.txt'
test_set.load_dataset(test_set_file, images_dir, annotations_dir)
test_set.prepare()

# plot predictions for train dataset
det.plot_actual_vs_predicted(train_set, model, cfg)
# plot predictions for test dataset
det.plot_actual_vs_predicted(test_set, model, cfg)
