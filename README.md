# README #

This README would normally document whatever steps are necessary to get your application up and running.

### INstallation for Mask-RCNN ###

1 - Instalação Anaconda
2 - conda create --name vending_gpu tensorflow-gpu=1.15.0
3 - Test gpu compatibility
	import tensorflow as tf
	sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
4 - pip install --no-deps keras==2.2.4
5 - git clone https://github.com/matterport/Mask_RCNN.git
6 - cd Mask_RCNN
7 - pip install -r requirements.txt ou pip3
8 - python setup.py install ou python3
9 - pip show mask-rcnn