from DetImages import DetDataset, DetConfig
from mrcnn.model import MaskRCNN
from keras.callbacks import Callback, EarlyStopping, ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau


images_dir = 'D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\JPEGImages'
annotations_dir ='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\Annotations'

train_set = DetDataset()

# train set
train_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\train.txt'
train_set.load_dataset(train_set_file, images_dir, annotations_dir)
train_set.prepare()

test_set = DetDataset()
test_set_file='D:\\deep_learning_vending_machines\\SmartUVM_Datasets\\UVM_Datasets\\voc8\\voc8\\VOCdevkit\\VOC2007\\ImageSets\\Main\\test.txt'
test_set.load_dataset(test_set_file, images_dir, annotations_dir)
test_set.prepare()

# prepare config
config = DetConfig()
config.display()

# define the model
model = MaskRCNN(mode='training', model_dir='./', config=config)
# load weights (mscoco) and exclude the output layers
model.load_weights('mask_rcnn_coco.h5', by_name=True, exclude=["mrcnn_class_logits", "mrcnn_bbox_fc",  "mrcnn_bbox", "mrcnn_mask"])

# train weights (output layers or 'heads')
#checkpoint_callback = ModelCheckpoint(filepath = filepath, save_best_only=True, monitor=monitor, save_weights_only=True)
#callbacks = [checkpoint_callback]
model.train(train_set, test_set, learning_rate=config.LEARNING_RATE, epochs=15, layers='heads')